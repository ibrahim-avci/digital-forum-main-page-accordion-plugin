import { withPluginApi } from "discourse/lib/plugin-api";


function initialize(api){
  api.modifyClass('component:categories-only', {
    didRender() {
      this.$('.subcategories').addClass('hidden');
      $('.toggleBtn-cat').on('click',function (e) {
        e.preventDefault();
        $(this).parent().find('.subcategories').toggleClass('hidden');
      });
    }
  });
}

function disabledInitialize(api){
  api.modifyClass('component:categories-only', {
    didRender() {
      $('.toggleBtn-cat').addClass('hidden');
    }
  });
}

export default {
  name: 'main-category',
  initialize(container) {
    const siteSettings = container.lookup("site-settings:main");
  if(siteSettings.main_category_enabled){
    withPluginApi("0.8.7", initialize);
  }else{
    withPluginApi("0.8.7", disabledInitialize);
  }}
};


